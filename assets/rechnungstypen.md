
Ein Rechnungstyp mit Umsatzsteuer:

  - **UST-7** - 7% Umsatzsteuer, der Normalfall, keine Produktvariante

Zwei Rechnungstypen ohne Umsatzsteuer:

Für diese Rechnungstypen muss die Produktvariante "Netto" verwendet werden. Dort ist der Preis ohne Umsatzsteuer hinterlegt.

  - **UST-0-EU-reverse-charge** - 0% Umsatzsteuer, wenn die Rechnung an ein Unternehmen in der EU geht. Die VAT ID des Unternehmens muss dafür in der Rechnungsgruppe hinterlegt sein. Produktvariante: Netto
  - **UST-0-nicht-EU** - 0% Umsatzsteuer, wenn die Rechnung an eine Privatperson oder ein Unternehmen außerhalb der EU geht. Produktvariante: Netto



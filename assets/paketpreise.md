Jährlich muss ein Abo pro aktivem Leser abgerechnet werden - mindestens.

 In einigen Fällen werden mehr Abos abgerechnet, weil das günstiger ist: 

   - 10er-Pakete lohnen sich ab 8 Lesern (8 Einzelabos wären teurer  als 10 Abos zum 10er-Paket-Preis)
   - 50er-Pakete lohnen sich ab 21 Lesern (21 Abos zum 10er-Paket-Preis wären teurer als 50 Abos zum 50er-Paket-Preis)

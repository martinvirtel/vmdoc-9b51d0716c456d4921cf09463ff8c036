Die Rechnungsadresse muss 4-5 Zeilen lang sein und ein Länderkürzel in der letzten Zeile haben - "DE", "AT", "CH" oder "GB" für Großbritannien. Diese Regel stammt von Lexoffice und sorgt dafür, dass ein korrekter Mehrwertsteuersatz in der Rechnung steht.


Beispiele:

Deutschland, 5 Zeilen + Länderkennzeichen in der letzten Zeile

```
Google Germany GmbH
Katrin Röske

ABC-Str. 19 
20354 Hamburg
DE
```


Großbritannien, 5 Zeilen + Länderkennzeichen in der letzten Zeile

```
Howden Group Holdings Limited
Helen Smith

One Creechurch Place / One Creechurch Ln 
EC3A 5AF London
GB
```


Deutschland, keine Leerzeile, 4 Zeilen + Länderkennzeichen in der letzten Zeile

```
Viridium Service Management GmbH / presse@viridium-gruppe.com
Sara Yussefi / Kostenstelle 11001400
Dornhofstrasse 36 
63263 Neu-Isenburg
DE
```


Komplette Liste der Länderkürzel, die Lexoffice zulässt:

CA CN FR DE HK HU IL IT JP KR NL PT RU SG ES TR VN GB US AF AL DZ AS AD AO AI AQ AG AR AM AW AU AT AZ BS BH BD BB BY BE BZ BJ BM BT BO BA BW BR VG BN BG BF MM BI KH CM CV KY CF TD CL CX CC CO KM CD CG CK CR HR CU CY CZ DK DJ DM DO EC EG SV GQ ER EE ET FO FK FJ FI GF PF GA GM GE GH GI GR GL GD GP GU GT GN GW GY HT HN IS IN ID IR IQ IE CI JM JO KZ KE KI KW KP KG LA LV LB LS LR LY LI LT LU MO MK MG MW MY MV ML MT MH MQ MR MU YT MX FM MD MC MN ME MS MA MZ NA NR NP AN NC NZ NI NE NG NU NF NO OM PK PW PS PA PG PY PE PH PL PR QA RE RO RW SH KN LC PM VC SM ST SA SN RS SC SL SK SI SB SO ZA LK SD SR SZ SE CH SY TW TJ TZ TH TG TO TT TN TM TC TV UG UA AE UY UZ VU VA VE WF EH YE ZM ZW AC AX BV DG GG GS HM IM IO JE MP PN SJ TA TF TK TL VI WS BL BQ CW MF SX XK ES_CN ES_CE ES_ML GR_69 XI

Codes mit einem "_" müssen verwendet werden, wenn ein Land steuerbegünstigte Gebiete hat. "ES_CN" steht zum Beispiel für die kanarischen Inseln, dort gelten andere Steuergesetze als auf dem spanischen Festland.

Bis auf wenige Ausnahmen verwendet Lexoffice die zwei-Buchstaben-Länderkürzel nach diesem Standard: https://de.wikipedia.org/wiki/ISO-3166-1-Kodierliste

Eine Erklärung für die Codes und die Anleitung, wie man eine Liste der Codes erhält, findet sich in der [Lexoffice-Dokumentation](https://developers.lexoffice.io/docs/#countries-endpoint).

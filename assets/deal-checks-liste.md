Fehler im Deal:

  - Der Deal enthält keine Produkte
  - Für den Deal gibt es bereits eine Lexware-Rechnung, die nicht storniert wurde
  - Der Rechnungstyp im Deal ist "UST-0-reverse-charge", aber die Rechnungsgruppe enthält keine VAT ID
  - Die Rechnungstyp im Deal enthält Umsatzsteuer, aber unter "Produkte" wurde die Variante "Netto" verwendet
  - Die Rechnungstyp im Deal enthält 0% Umsatzsteuer, aber unter "Produkte" wurde  die Variante "Netto" nicht verwendet

Fehler in der Rechnungsgruppe:

  - Die Rechnungsgruppe hat keine Rechnungsadresse
  - Die Rechnungsadresse endet nicht mit einem der erlaubten Länderkürzel
  - Die Rechnungsgruppe hat den Abrechnungstyp "nicht abrechnen" oder "deaktiviert" 
  - Die Rechnungsgruppe hat kein Datum unter "Nächste Abo-Abrechnung", der Deal hat kein Startdatum und keine Enddatum

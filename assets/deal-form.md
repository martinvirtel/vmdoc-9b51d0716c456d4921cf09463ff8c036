|     |     |
| --- | --- |
| Titel nach Schema <br/><span style="border: 1px solid grey; padding: 0.2em;  "><i>RE Jahr</i> (Name der Rechnungsgruppe) | <input type="text" placeholder="RE 2029 RG Disney Re / B10 / 03.08."> |
| Wie viele Abos müssen abgerechnet werden? | <input type="text" placeholder="1"> |
| Welches Produkt? | <input type="text" placeholder="50+"> |
| Welcher Rechnungstyp? | <input type="text" placeholder="UST-7"> |
| Startdatum (Datum) | <input type="text" placeholder="10. 8. 2023"> |
| Enddatum (Datum) | <input type="text" placeholder="10. 8. 2023"> |
| Welche Personen? | <input type="text" placeholder=" "> |

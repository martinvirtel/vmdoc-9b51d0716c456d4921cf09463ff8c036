Um die Übersicht zu erleichtern, werden Rechnungsgruppen nach diesem Schema benannt:

<span style="border: 1px solid grey; padding: 0.2em;  ">
RG <i>Unternehmen oder Person</i> / <b>Produktkürzel</b> / Abrechnungstermin 
</span>

Zum Beispiel:

<span style="border: 1px solid grey; padding: 0.2em;  ">
RG <i>Swiss Re Europe</i> / <b>A 10</b> / 01.06.
</span>

Wenn der Firmenname in vielen Rechnungsgruppen vorkommt, besteht Verwechselungsgefahr. Dann muss der Name 
noch detaillierter ausfallen. Beispiele:

<p>
<span style="border: 1px solid grey; padding: 0.2em;  ">
RG <i>Disney Re Europe</i> / <b>A 10</b> / 03.06. / Kostenstelle Dagobert Duck
</span>
</p>

<p>
<span style="border: 1px solid grey; padding: 0.2em;  ">
RG <i>Disney Re Europe</i> / <b>A 10</b> / 01.06. / Kostenstelle Goofy
</span>
</p>

+++
title = "Rechnungen in Lexoffice" 
weight = 5
+++

# Rechnungen in Lexoffice

Sobald ein Deal in der Phase "Lexoffice" angekommen ist, werden die Daten von Pipedrive zu Lexoffice übertragen, und es wird ein Beleg (Rechnung oder Rechnungskorrektur) mit diesen Daten angelegt und eine Belegnummer vergeben. Der Vorgang dauert 30-90 Sekunden, der Vorgang wird in Pipedrive protokolliert, indem eine Notiz an den Deal angeheftet wird. 

Wenn der Beleg in Lexoffice als Entwurf angelegt wurde, wird in der Notiz der entsprechende Link hinterlegt. Der Beleg hat in Lexoffice den Status "Entwurf". In Pipedrive trägt der Deal dann ebenfalls das Etikett "Entwurf", in der Titelzeile der Notiz erscheint "draft".

## Abschließen in Lexoffice

In Lexoffice kann der Entwurf noch einmal geprüft, eventuell noch korrigiert und ergänzt werden, um ihn dann abzuschließen. Ein abgeschlossener Beleg hat in Lexoffice den Zustand "offen" - klingt verwirrend, das Wort bezieht sich auf die Zahlung, die noch offen ist. Das "Offen" wird auch durch ein Etikett am Deal signalisiert, außerdem wandert der Deal, zu dem der Beleg gehört, durch das Abschließen in die letzte Spalte der Pipeline, "Abgeschlossen". In der Notiz erscheint das Wort "abgeschlossen".  

Eine veränderte Rechnungsadresse, ein neuer Ansprechpartner und alle anderen Korrekturen, die Auswirkungen auf die nächsten Rechnungen für diesen Kunden haben, müssen auch in der zugehörigen Rechnungsgruppe in Pipedrive umgesetzt werden, um künftige Abrechnungen für diesen Kunden zu erleichtern.

## Stornieren in Lexoffice

Nach dem Zustand "offen" folgt in Lexoffice entweder "bezahlt" oder "storniert". Stornieren ist Pflicht, wenn ein Beleg im Zustand "Offen" 
oder "Bezahlt" noch einmal korrigiert werden muss.

Durch das Stornieren wird der Deal, der zu einem Beleg gehört, in die Phase "in Arbeit" zurüchversetzt. Außerdem wird das Etikett "Storno" zum Deal hinzugefügt. Auch die Notiz wird um das Wort "storno" ergänzt.

## Briefpapier ändern

Briefpapier ist ein PDF in Größe DIN A4, das man in Lexoffice hochladen muss. Lexoffice hat auch eine Anleitung dafür: [Rechnungsvorlage: Firmenlogo und Briefpapier anpassen | lexoffice Help Center](https://support.lexoffice.de/de-form/articles/2112411-rechnungsvorlage-firmenlogo-und-briefpapier-anpassen).

Die Einzelschritte:

1. Lexoffice öffnen, dort einen Rechnungsentwurf auswählen, "bearbeiten", dann "weiter zur Vorschau" 

Unter "Entwürfe" gibt es zum Beispiel ein paar Testrechnungen, die wir niemals abschließen, genau für diesen Zweck.

  {{< img src="01-briefpapier" size="fit 400x200" >}}
  {{< img src="02-briefpapier" size="fit 400x200" >}}
  {{< img src="03-briefpapier" size="fit 400x200" >}}


2. Bei der gewünschten Vorlage das &vellip; - Menü öffnen, "Bearbeiten" auswählen, dann links "Logo und Briefpapier" wählen.
  
  {{< img src="04-briefpapier" size="fit 400x200" >}}
  {{< img src="05-briefpapier" size="fit 400x200" >}}


3. Altes Briefpapier entfernen, neues hochladen, speichern. 
  {{< img src="06-briefpapier" size="fit 400x200" >}}
  {{< img src="07-briefpapier" size="fit 400x200" >}}

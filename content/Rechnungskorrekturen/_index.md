+++
title = "Rechnungskorrekturen"
weight = 5
+++

# Rechnungskorrekturen

Rechnungskorrekturen werden ganz ähnlich wie Rechnungen vorbereitet und erstellt. 

Start ist in der zugehörigen Rechnungsgruppe. Dort bitte "Deal hinzufügen" klicken. 

Der wichtigste Unterschied zu Rechnungen ist, dass sie in der Pipeline "Rechnungskorrekturen" angelegt werden, nicht in der Pipeline "Rechnungen".


Sie werden als **in Arbeit** angelegt, dann geprüft und in Phase **geprüft** verschoben. Durch Verschieben in die Phase **Lexoffice** wird ein Rechnungsentwurf in Lexoffice angelegt. 

{{< img src="mermaid-diagram-2023-07-28" size="fit 900x600" css=" float: left;" >}}




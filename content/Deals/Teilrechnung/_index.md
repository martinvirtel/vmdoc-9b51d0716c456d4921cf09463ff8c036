+++
title = "Deal für Teilrechnung"
+++



# 


# Bitte vorab klären

{{< noteform >}}

|     |     |
| --- | --- |
| Titel nach Schema <br/><span style="background: lightgrey; padding: 0.1px;  "><i>RT </i><b> Stichwort </b> (Name der Rechnungsgruppe)</span> | <input type="text" placeholder="RT Abo Goofy RG Disney Re / B10 / 03.08."> |
| Wie viele Abos müssen abgerechnet werden? | <input type="text" placeholder="1"> |
| Welcher Rechnungstyp? | <input type="text" placeholder="UST-7"> |
| Welches Produkt? | <input type="text" placeholder="50+"> |
| Startdatum (Datum) | <input type="text" placeholder="10. 8. 2023"> |
| Enddatum (Datum) | <input type="text" placeholder="10. 8. 2023"> |
| Anteiliger Preis   | <input type="text" placeholder="30.25 €, Preis für 10 Tage "> |
| Welche Teilnehmer? | <input type="text" placeholder=" "> |

Siehe {{< include "link-abopreis-rechner.md" >}}

{{< /noteform >}}


# Arbeitsschritte

  *  <input type="checkbox"/> Rechnungsgruppe wählen 
  *  <input type="checkbox"/> "Deal hinzufügen"
  *  <input type="checkbox"/> Titel nach Schema <span style="background: lightgrey; padding: 0.1px;  "><i>RT </i><b> Stichwort </b> (Name der Rechnungsgruppe)</span> 
  *  <input type="checkbox"/> Pipeline "Rechnungen" wählen
  *  <input type="checkbox"/> Rechnungstyp wählen (-> [Details](#rechnungstypen))
  *  <input type="checkbox"/> Teilpreis berechnen
  *  <input type="checkbox"/> Produkte hinzufügen (-> [Details](#produkte)), Variante "Netto" bei Rechnungstyp mit 0% UsT
  *  <input type="checkbox"/> Teilnehmer zum Deal hinzufügen (-> [Details](#teilnehmer))
  *  <input type="checkbox"/> Start / Enddatum hinzufügen (-> [Details](#produkte)), Variante "Netto" bei Rechnungstyp mit 0% UsT
  *  <input type="checkbox"/> Speichern
  *  <input type="checkbox"/> Prüfen (-> [Checkliste](#checkliste)) und Pipeline-Phase "geprüft" einstellen 


<div style="height: 0px; visibility: hidden;">

# Screencast: Deal für Rechnung Vorbereiten

{{< video src="rechnungsgruppe_anlegen" width="60%" muted="true" >}}

{{< include "video-explainer.md" >}}

</div>

# Details

## Rechnungstypen

Wir arbeiten mit drei Rechnungstypen:

{{< include "rechnungstypen.md" >}}

## Produkte 

{{< include "paketpreise.md" >}}

## Teilnehmer

Normalerweise werden alle Mitglieder einer Rechnungsgruppe auf der Rechnung aufgelistet. Bei Teilrechnungen wird aber häufig nur für neue
Mitglieder abgerechnet. In diesem Fall müssen die Teilnehmer für diese Teilrechnung direkt dem Deal hinzugefügt werden.

{{<img src="piedrive_deal_teilnehmer.png" >}}

## Checkliste

Wenn alle Angaben vollständig sind, kann der Deal in die Phase "geprüft" übergehen. 

Es gibt eine Reihe von automatischen Prüfungen, die dazu führen können, dass der Deal von Lexoffice 
nicht in eine Rechnung umgewandelt werden kann:

{{< include "deal-checks-liste.md" >}}

+++
title = "Deal für Jahresrechnung"
+++



# 


# Bitte vorab klären

{{< noteform >}}

|     |     |
| --- | --- |
| Titel nach Schema <br/><span style="background: lightgrey; padding: 0.1px;  "><i>RE Jahr</i> (Name der Rechnungsgruppe)</span> | <input type="text" placeholder="RE 2029 RG Disney Re / B10 / 03.08."> |
| Wie viele Abos müssen abgerechnet werden? | <input type="text" placeholder="1"> |
| Gibt es unbesetzte Abos? | <input type="text" placeholder="1"> |
| Welches Produkt? | <input type="text" placeholder="50+"> |
| Welcher Rechnungstyp? | <input type="text" placeholder="UST-7"> |
| Startdatum (Datum) | <input type="text" placeholder="10. 8. 2023"> |
| Enddatum (Datum) | <input type="text" placeholder="10. 8. 2023"> |
| Welche Personen? | <input type="text" placeholder=" "> |

{{< /noteform >}}

</div>

# Arbeitsschritte

  * <input type="checkbox"/> Rechnungsgruppe wählen 
  * <input type="checkbox"/> "Deal hinzufügen"
  * <input type="checkbox"/> Titel nach Schema <span style="background: lightgrey; padding: 0.1px;  "><i>RE Jahr</i> (Name der Rechnungsgruppe)</span>
  * <input type="checkbox"/> Pipeline "Rechnungen" wählen
  * <input type="checkbox"/> Rechnungstyp wählen (-> [Details](#rechnungstypen))
  * <input type="checkbox"/> Produkte hinzufügen (-> [Details](#produkte)), Variante "Netto" bei Rechnungstyp mit 0% UsT, Anzahl ist die Anzahl der abzurechnenden Abos.
  * <input type="checkbox"/> Wenn es unbesetzte Abos gibt: Freitext auf Rechnung hinzufügen, Muster "Ein Abo ist noch zu besetzen"
  * <input type="checkbox"/> Speichern
  * <input type="checkbox"/> Prüfen (-> [Checkliste](#checkliste)) und Pipeline-Phase "geprüft" einstellen 


**Teilnehmer**, **Startdatum** und **Enddatum** werden bei Jahresrechnungen automatisch ausgefüllt, wenn nichts anderes eingetragen ist.

Datums-Automatik: Wenn Start- und Enddatum nicht eingetragen werden, setzt das System automatisch den nächsten möglichen Abrechnungstermin als Startdatum, das Enddatum entsprechend einen Tag vor dem übernächsten Abrechnungstermin.

Teilnehmer-Automatik: Alle Abonnenten in der Rechnungsgruppe werden der Rechnung hinzugefügt, wenn sie zwischen Start- und Enddatum aktiviert sind.


<div style="height: 0px; visibility: hidden;">

# Screencast: Deal für Rechnung Vorbereiten

{{< video src="rechnungsgruppe_anlegen" width="60%" muted="true" >}}

{{< include "video-explainer.md" >}}


# Details

## Rechnungstypen

Wir arbeiten mit drei Rechnungstypen:

{{< include "rechnungstypen.md" >}}

## Produkte 

{{< include "paketpreise.md" >}}

## Checkliste

Wenn alle Angaben vollständig sind, kann der Deal in die Phase "geprüft" übergehen. 

Es gibt eine Reihe von automatischen Prüfungen, die dazu führen können, dass der Deal von Lexoffice 
nicht in eine Rechnung umgewandelt werden kann:

{{< include "deal-checks-liste.md" >}}

+++
title = "Deals in Pipedrive" 
weight = "4"
+++





### Rechnungen, Deals und Pipelines

Jede Rechnung und (und auch jede Rechnungskorrektur) für Abos werden als ein Deal in Pipedrive angelegt. Der Deal enthält alle Daten, die für die Erstellung der Belege nötig sind. 


Bester Startpunkt dafür ist die Rechnungsgruppe, zu der diese Rechnung bzw. Rechnungskorrektur gehört. 

Rechnungen werden in der [Pipeline "Rechnungen"](https://frommesversicherungsmonitorgmbhabo.pipedrive.com/pipeline/1/user/everyone) angelegt, es gibt weitere Pipelines für Rechnungskorrekturen und Kündigungen.

Rechnungen werden als **in Arbeit** angelegt, dann geprüft und in Phase **geprüft** verschoben. Durch Verschieben in die Phase **Lexoffice** wird ein Rechnungsentwurf in Lexoffice angelegt. 

{{< img src="mermaid-diagram-2023-07-28" size="fit 600x600" css=" float: left;" >}}

<br clear="all">

Hier geht es zur {{< include "link-pipeline-rechnungen.md" >}}.

## Gewonnen / Verloren

In Pipedrive können Deals als "gewonnen" oder "verloren" markiert werden. 

{{< img src="verloren" size="fit 300x300" css=" float: left;" >}}

Deals, die absehbar nicht mehr zu Rechnungen werden - zum Beispiel, weil es rechtzeitig eine Kündigung gab - sollten mit "Verloren" markiert werden. 
Bitte als Verlustgrund dann den Grund angeben, warum keine Rechnung gestellt wird. 

"Gewonnen" bedeutet - alles erledigt, es gibt eine Rechnung. Es gibt eine Aufräum-Automatik: Ein Deal wird automatisch als **gewonnen** markiert, wenn er 1. Teil einer Jahresrechnungs-Aktivität ist, 2. eine Lexware-Rechnung abgeschlossen wurde und 3. der Deal bereits länger als zwei Tage in der Phase "abgeschlossen" ist. Es spricht aber nichts dagegen, einen Deal manuell auf "gewonnen" zu stellen, sobald alles erledigt ist.




<div style="visibility: hidden; height: 0;">
flowchart LR
  subgraph Lexoffice
    F[Entwurf] --> G[Versandfertig]
    G --> H[Bezahlt]
    G --> I[Storno]
  end
  subgraph Pipedrive
    A[In Arbeit] --> B[Geprüft]
    B --> C
    C --> D[Lexoffice]
    D --> E[Abgeschlossen]
    C((d)) --> F
  end
</div>



### Rechnungstyp (Steuern)

Wir arbeiten mit drei Rechnungstypen:

{{< include "rechnungstypen.md" >}}


### Teilrechnungen und Jahresrechnungen

Abos laufen in der Regel 12 Monate und verlängern sich automatisch. Hier werden Jahresrechnungen geschrieben.

Wenn Abonnenten neu hinzukommen, werden sie wenn sinnvoll zu bestehenden Rechnungsgruppen hinzugefügt, um die Abrechnung zu erleichtern.  In diesem
Fall wird eine Teilrechung angelegt - bezahlt werden die Tage bis zum nächsten Abrechnungstermin, danach wird er wie alle anderen in der Rechnungsgruppe jährlich abgerechnet.


## Vom Deal zur Rechnung

Wenn alle Angaben vollständig sind, kann der Deal in die Phase "geprüft" übergehen. Es gibt eine Reihe von automatischen Prüfungen, die dazu führen können, dass der Deal nicht in eine Rechnung umgewandelt werden kann:

{{< include "deal-checks-liste.md" >}}

Danach geht's weiter mit der [Weiterverarbeitung in Lexoffice](../lexoffice)

## Schritt für Schritt: Teilrechnung anlegen 

Funktioniert so wie die Jahresrechnung, mit diesen Erweiterungen: 

  - der Zeitraum muss eingetragen werden
  - der Preis muss separat berechnet werden
  - die Personen auf der Rechnung müssen separat benannt werden

**Vorab** klären: 

  1. wie viele Abos müssen abgerechnet werden?
  2. für welchen Zeitraum?
  3. zu welchem Tarif?
  4. Welcher Rechnungstyp?
  5. Welche Personen?


Die Schritte:

  - [ ] Rechnungsgruppe wählen 
  - [ ] "Deal hinzufügen"
  - [ ] Namen nach Schema vergeben, mit Jahr und Monat "ReT 2024-07 " + Namen der Rechnungsgruppe 
  - [ ] Pipeline "Rechnungen" wählen
  - [ ] Teilpreis ausrechnen
  - [ ] Abos (=Produkte) zum Teilpreis hinzufügen unter "Produkte" hinzufügen
  - [ ] Rechnungstyp wählen
  - [ ] "Startdatum" und "Enddatum" eintragen.
  - [ ] Personen, auf die sich die Teilrechnung bezieht, unter "Teilnehmer" hinzufügen


Wenn alle Angaben vollständig sind, kann der Deal in die Phase "geprüft" übergehen. Es gibt eine Reihe von automatischen Prüfungen, die dazu führen können, dass der Deal nicht in eine Rechnung umgewandelt werden kann:

{{< include "deal-checks-liste.md" >}}



<div style="height:3em; width:100%;" id="anleitung-lexoffice"></div>

## Weiterverarbeitung in Lexoffice

Sobald ein Rechung in der Phase "Lexoffice" steckt, wird der Deal als Rechnung übertragen. Für Rechnungskorrekturen in ihren jeweiligen Phasen gilt dasselbe.

Die Übermittlung wird in einer Notiz im Deal protokolliert. Zunächst steht dort "deal-o-mat für Rechnung gestartet", nach einer kurzen Zeit steht dann das Ergebnis der Übermittlung dort: entweder ein grüner Haken -> Die Rechnung in Lexoffice ist im Status "Entwurf" angelegt, oder ein rotes "x", weil ein Fehler passiert ist.

Bei Fehlern muss der Deal zurück in die Phase "in Arbeit", dann muss der Fehler behoben werden, und dann kann es weitergehen.

In Lexoffice hat jede Rechnung und jeder Rechnungsentwurf einen von vier Zuständen:

  - Entwurf (Pipedrive-Etikett: Entwurf, Pipedrive-Phase "Lexware")
  - Offen (Pipedrive-Etikett: Abgeschlossen, Pipedrive-Phase "Abgeschlossen")
  - Bezahlt (Pipedrive-Etikett: Bezahlt, Pipedrive-Phase "Abgeschlossen")
  - Storno (Pipedrive-Etikett: Storno, Pipedrive-Phase "In Arbeit")

Wenn sich der Zustand in Lexoffice ändert, wird in Pipedrive ein entsprechendes Etikett vergeben, außerdem wird der Deal in die entsprechende Pipeline-Phase verschoben.

### Fehler bei der Rechnungserstellung 

Vor Erstellung der Rechnung werden einige Details im Deal überprüft. Wenn die nicht stimmen, schlägt die Rechungserstellung fehl.

{{< include "deal-checks-liste.md" >}}



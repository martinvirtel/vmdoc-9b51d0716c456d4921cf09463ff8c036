+++
title = "Produkte" 
weight = 2
hidden = false
+++

Um auch auf Rechnungen ohne Mehrwertsteuer korrekt verrechnet werden zu können, muss für alle unsere Produkte in Pipedrive eine Variante mit dem Namen "Netto" eingerichtet werden. 

Diese Variante muss verwendet werden, wenn die Rechnung vom Typ UST-0-reverse-charge oder UST-0-non-eu ist. 

Die Rechnungstypen:

{{< include "rechnungstypen.md" >}}

Das Anlegen einer Variante ist im Video ab ca. 0:27 zu sehen. Die Varianten heißen "2023 Brutto" oder so ählich - erst das Jahr, ab dem sie gelten, dann das Wort "Brutto" oder "Netto".

Bei Produkten, für die Mindestmengen gelten, muss das Feld "Mindestmenge" ausgefüllt werden, um die Berechnung von Prognosen zu vereinfachen. Zum Beispiel "50" für 50-er Abos. 

### Screencast: Produkt anlegen

{{< video src="produkt_anlegen" width="60%" muted="true" >}}

{{< include "video-explainer.md" >}}

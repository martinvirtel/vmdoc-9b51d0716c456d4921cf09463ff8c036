+++
title = "Pipedrive intern"
weight = 9
+++

# Pipedrive intern

## Pipedrive-Anpassungen

Unten sind alle Anpassungen aufgelistet, die für den Versicherungsmonitor in Pipedrive gemacht wurden. 


### Organisationen

Etiketten:

  - "Unternehmensgruppe" - ID 16
  - "Rechnungsgruppe" - ID 15

Felder:


| Name | Typ |  |
| ----- | ----- | ----- |
| Abzurechnende Abos | Text | für Abo-Verlängerungen: Wie viele Abos müssen verlängert werden |
| Unbesetzte Abos | Text | für Abo-Verlängerungen: Wie viele Abos sind nicht durch Rechnungsgruppen-Mitglieder besetzt |
| Ansprechpartner | Kontakt | Text auf Rechnung | 
| Hinweis auf Rechnung | Großtext | |
| Kunden-Kategorie | Einzeloption |Erstversicherer, Rückversicherer, Vermittler, Verbände und Behörden, Sonstige |
| Lexoffice Kunde | Text | |
| Nächste Abo-Abrechnung | Datum | |
| PMP Name | Text | |
| Rechnungsadresse | Großtext | |
| Abrechnung | Einzeloption | Jährlich, Deaktiviert, Nicht abrechen |
| Unternehmensgruppe | Organisation | |
| VAT ID | Text | |
| Wordpress_ID | Text | |


### Kontakte

| Name | Typ |  |
| ----- | ----- | ----- |
| Aktiviert ab | Datum | |
| Deaktiviert nach | Datum | |
| Email-Abos | Mehrfachoption | Monitor am Morgen, Die Woche |
| Ersatzleser für | Text | |
| Lexoffice Kontakt | Text | |
| Lieferadresse | Großtext | |
| Titel | Text | |
| Wordpress_ID | Text | |


### Deals

Pipelines:

   - Rechnungen
      - In Arbeit
      - Geprüft
      - Lexoffice
      - Abgeschlossen

   - Rechnungskorrekturen
      - In Arbeit
      - Geprüft
      - Lexoffice
      - Abgeschlossen

   - Kündigungen
      - In Arbeit
      - Abgeschlossen

Etiketten:

   - Entwurf
   - Abgeschlossen
   - Bezahlt
   - Storno


Felder:

| Name | Typ |  |
| ----- | ----- | ----- |
| Ansprechpartner | Kontakt | |
| Belegnummer | Text | |
| Enddatum | Datum | |
| Freitext auf Rechnung | Großtext | |
| Korrektur für Rechnung | Text | |
| Kündigungskategorie | Mehrfachoption | Nachhaken, Nicht nachhaken, Nicht nachhaken (Kulanz) |
| Lexoffice | Text | |
| Rechnungstyp | Einzeloption | |
| Startdatum | Datum | |


### Aktivitäten

Typen:

  - Jahresrechnung
  - Neue Organisation


### Produkte

Felder:

| Name | Typ |  |
| ----- | ----- | ----- |
| Nur für Organisation | Organisation | wird verwendet, wenn ein bestimmter Tarf nur für eine Organisation verwendet wird |
| Mindestmenge | Zahl | Zur Kontrolle, ob tatsächlich so viele Abos bestellt wurden |


Varianten:


| Name | Verwendung |
| ----- | ----- |
| Netto | enthält den Netto-Preis (ohne Umsatzsteuer) für ein Produkt |


### Automations



#### Organisation gewechselt

Jedes Mal, wenn ein Kontakt von einer Organisation in eine andere verschoben wird, werden zwei erledigte Aktivitäten angelegt, um das zu dokumentieren.

{{<img src="automation-nutzer-wechselt-organisation" >}}


#### Neue Organsiation angelegt

Jedes Mal, wenn ein Kontakt neu in einer Organisation angelegt wird, wird eine erledigte Aktivitäten angelegt, um das zu dokumentieren.

{{<img src="nutzer-in-organisation-angelegt.png" >}}

#### Ersatzleser zugewiesen

Jedes Mal, wenn einem Kontakt ein Ersatzleser zugewiesen wird, wird eine erledigte Aktivitäten angelegt, um das zu dokumentieren.

{{<img src="ersatzleser-zugewiesen.png" >}}

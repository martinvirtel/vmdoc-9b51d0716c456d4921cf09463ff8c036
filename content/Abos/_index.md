+++
title = "Abos"
weight = 2
+++

# Abos, Kontakte und Rechnungsgruppen

Die Abos unserer Leserinnen und Leser werden als Kontakte in Pipedrive verwaltet. Die Kontakte sind in Rechnungsgruppen zusammengefasst.

Wer nicht in einer Rechnungsgruppe ist, kann versicherungsmonitor.de nicht lesen. Deswegen gibt es auch eine Rechnunsgruppe für Mitarbeiter und Menschen, die z.B. bei poolie studios für uns Software schreiben, und die kein Abo haben.

Rechnungsgruppen werden als Organisationen in Pipedrive gespeichert. Diese Organisationen haben das Etikett 'Rechnungsgruppe', um sie von anderen Arten von Organisationen zu unterscheiden, zum Beispiel von [Unternehmensgruppen](../rechnungsgruppen/#unternehmensgruppen).

Gespeichert werden für jeden Leser Namen, E-Mail,  Postadresse für die Zustellung von Dossiers, der Abo-Status für 'Monitor am Morgen' und 'Die Woche', ein 'aktiviert ab'-Datum und bei Bedarf auch ein 'deaktiviert nach'-Datum.
 

<div style="height:3em; width:100%;" id="anleitung-leseberechtigung"></div>

## Leseberechtigung 

 Ein Kontakt hat Leseberechtigung (und alle anderen Abo-Berechtigungen), wenn dieser Kontakt

  - zu einer Organisation gehört, die das Etikett "Rechnungsgruppe" hat
  - eine E-Mail-Adresse hat
  - das 'aktiviert ab'- Datum erreicht oder überschritten ist
  - kein 'deaktiviert nach'-Datum eingetragen ist oder das 'deaktiviert nach'-Datum noch nicht vorbei ist.

Für die Leseberechtigung ist der Abrechnungstyp einer Rechungsgruppe egal. Das bedeutet: wenn eine Rechnungsgruppe auf "deaktiviert" gesetzt wird, sind die verbleibenden Mitglieder weiter leseberechtigt. 

Um die Leseberechtigung tatsächlich zu entziehen, muss für die Mitglieder ein "deaktiviert nach"-Datum gesetzt werden. 

<div style="height:3em; width:100%;" id="anleitung-leser"></div>

## Schritt für Schritt: Leser anlegen 

**Vorab** Klären, welcher Rechnungsgruppe der neue Leser abgerechnet werden soll.

Wenn der neue Abonnent einer neuen Rechnungsgruppe angehören soll, bitte zuerst [eine neue Rechnungsgruppe anlegen](../rechnungsgruppen/#anleitung-rechnungsgruppe).

Wenn sich die Rechnungsgruppe nicht rechtzeitig klären lässt, kann der neue Leser vorübergehend in der -> Rechnungsgruppe 'Leseberechtigung - noch nicht zugeordnet' angelegt werden: 
[https://frommesversicherungsmonitorgmbhabo.pipedrive.com/organization/1270](https://frommesversicherungsmonitorgmbhabo.pipedrive.com/organization/1270)

Dann geht es los:

- [ ] Rechnungsgruppe aufrufen
- [ ] '+' in der Mitgliederliste klicken 
- [ ] Namen und E-Mail-Adresse eintragen
- [ ] Unter "E-Mail-Abos" die Abos für 'Monitor am Morgen' und 'Die Woche' aktivieren, wenn vom Leser nichts anderes gewünscht ist
- [ ] 'aktiviert ab'-Datum eintragen. Wenn der Kontakt sofort eine Leseberechtigung haben soll, dann muss hier das Datum von heute stehen
- [ ] wenn das Abo ausnahmsweise nicht automatisch verlängert werden soll: 'deaktiviert nach'-Datum eintragen.
- [ ] wenn die Zahl der abzurechnenden Abos in der Rechnungsgruppe durch die neue Abonnentin steigt: **+1 Abo = [Gesamtzahl der Abos] /"** den Titel der automatischen Aktivität hinzufügen und die Zahl der abzurechnenden Abos in der Rechnungsgruppe erhöhen

{{< img src="aktivitaet_plus_ein_abo.png" size="fit 600x300" css="float: left;" >}}


### Lieferadresse

Die Lieferadresse muss in einem speziellen Format eingetragen werden, damit die Postadressen korrekt gedruckt werden können. Für jede Zeile der Adresse gibt es einen Vorspann:


(Kopiervorlage)

```
t:Titel
v:Vorname
n:Nachname
f:Firma
s:Str./Hausnr
o:PLZ/Ort
l:Länderkürzel
```


Also zum Beispiel

```
t:
v:Reiner
n:Schwinger
f:Willis Towers Watson Versicherungsmakler GmbH
s:Welfenstr. 4
o:65189 Wiesbaden
l:DE
```


## Abrechnung von neuen Lesern

Eventuell muss für den neuen Leser auch eine neue Rechnung vorbereitet werden. Wenn mehrere  Neuzugänge in einer Rechnungsgruppe angelegt werden, ist es meist möglich, eine Rechnung für alle Neuzugänge vorzubereiten.

Eine Rechnung ist in zwei Fällen nötig:

  - wenn eine neue Rechnungsgruppe für den Leser erstellt wurde, muss eine Jahresrechnung vorbereitet werden

  - wenn sich durch den neuen Leser die Zahl der abzurechnenden Abos in der Rechnungsgruppe erhöht, muss eine Teilrechnung bis zum nächsten Abrechnungstermin der Rechnungsgruppe vorbereitet werden, um die Lücke zu füllen, siehe [Teilrechnung](../deals/teilrechnung).


Keine separate Rechnung ist nötig:

  - wenn in der Rechnungsgruppe noch freie Abos verfügbar waren oder der neue Leser einen alten ersetzt. Dann ist das Abo ja schon bezahlt.

  - wenn der neue Leser erst mit der nächsten Jahresrechnung aktiviert wird. Dann kann die Bezahlung ja im Regelfall warten.



## Leserin deaktivieren

Leserinnen und Leser sollten nicht gelöscht werden, sondern nur deaktiviert, damit alles nachvollziehbar bleibt.

- [ ] Leser aufrufen
- [ ] 'deaktiviert ab'- Datum eintragen. Wenn die Deaktivierung sofort wirksam sein soll, muss hier das Datum von gestern stehen. 
- [ ] Wenn sich die -> Zahl der abzurechnenden Abos durch die Deaktivierung verringert, weil es sich um eine Kündigung handelt, muss die Zahl der abzurechnenden Abos in der Rechnungsgruppe geändert werden.

Manchmal wird ein Teil des bezahlten Abos zurückerstattet. Das ist unter [Rechnungskorrektur](../rechnungskorrekturen) beschrieben.

{{< img src="nutzer-start-enddatum.png" size="fit 300x300" css="float: left;" >}}



## Kündigung

Kündigungen werden als Deals in der Pipeline "Kündigungen" angelegt. Die Pipeline hat nur zwei Phasen - "in Arbeit" und "abgeschlossen".

Schritt für Schritt:

- [ ] Rechungsgruppe aufrufen
- [ ] Deal hinzufügen
- [ ] Name nach Schema vergeben: "K " + Name der Rechnungsgruppe
- [ ] Produkte hinzufügen. Als Preis wird der bisher jährlich gezahlte Preis eingetragen, Anzahl ist die Anzahl der gekündigten Abos
- [ ] Deaktivierungsdatum (letzter Tag des Abos), eintragen unter "Startdatum"
- [ ] Datum, bis zu dem der Vertrag gelaufen wäre, eintragen unter "Enddatum". Im Normalfall bei fristgerechten Kündigungen dasselbe wie das Startdatum.
- [ ] Pipeline "Kündigungen" wählen
- [ ] Wenn sich die -> Zahl der abzurechnenden Abos durch die Kündigung verringert, muss diese Zahl in der Rechnungsgruppe  geändert werden. Bitte durch eine Aktivität protokollieren, in der "-1 Abo = XX Abos" festgehalten wird.
- [ ] Wenn die Rechnungsgruppe nach der Kündigung keine Abonnenten mehr enthält, kann sie deaktiviert werden. Dazu muss unter "Abrechnung" der Wert "deaktiviert" eingetragen werden.
- [ ] Wenn ausnahmsweise Geld zurückerstattet wird, müss eine Rechnungskorrektur geschrieben in der Rechnungsgruppe geschrieben werden.


Wenn alles komplett ist, kann der Deal in die Phase "abgeschlossen" weitergeschoben werden.


## Änderung der Pipeline

Wenn ein Deal versehentlich in der falschen Pipeline gelandet ist, kann er leicht in die richtige Pipeline verschoben werden.

 
{{< img src="pipeline-aendern.png" size="fit 300x300" css="float: right;" >}}


## Neue E-Mail-Adresse

Wenn ein Leser seine E-Mail-Adresse ändern möchte, kann dies einfach in Pipedrive geschehen. Die Änderung wirkt sich sofort aus. Das Passwort ändert sich dadurch nicht.


Die neue E-Mail-Adresse:

   * kann sofort verwendet werden, um sich mit dem selben Passwort wie vorher zum Lesen bei versicherungsmonitor.de anzumelden  
   * kann sofort verwendet werden, um ein neues Passwort zu setzen
   * gilt für die bestehenden Abos von "Monitor am Morgen" und "Die Woche"

Die E-Mail-Adressenänderung bitte als Aktivität protokollieren.


Ein potenzieller Stolperstein: Oft ist der Benutzername identisch mit der E-Mail-Adresse - die Änderung des Benutzernamens muss durch ein Ticket bei Poolie Studios veranlasst werden, 
das kann etwas länger dauern. Der Benutzername wird selten verwendet, deswegen kann diese Änderung nach der E-Mail-Adressenänderung geschehen.


Am Besten ist es, die Änderung der E-Mail-Adresse zu bestätigen, indem man dem Leser eine E-Mail an seine neue Adresse schreibt. Formulierungsvorschlag:

```

Sehr geehrte XXXXX,

ihr Abo ist jetzt auf die E-Mail YYYYYY umgestellt. Ab sofort können Sie sich mit dieser E-Mail bei versucherungsmonitor.de anmelden.

Wenn Sie unsere Newsletter "Monitor am Morgen" oder "Die Woche" abonniert haben, werden die in Zukunft auch an die neue Adresse verschickt.

Mit freundlichen Grüßen,

ZZZZZ


```


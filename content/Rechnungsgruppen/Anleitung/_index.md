+++
title = "Rechnungsgruppe anlegen"
+++

<div style="width: 60%">

{{< noteform >}}

# 

# Bitte notieren

|     |     |
| --- | --- |
| Unternehmen oder Person | <input type="text" placeholder="Disney Re"> |
| Produktkürzel  | <input type="text" placeholder="B10"> |
| Start ab (Datum) | <input type="text" placeholder="10. 8. 2023"> |
| Wie viele Abos?  | <input type="text" placeholder="8"> |
| Name nach Schema (-> [Details](#namensschema)) <br/><span style="border: 1px solid grey; padding: 0.2em;  "> RG <i>Name</i> / <b>Tarif</b> / Abrechnungstermin</span> | <input type="text" placeholder="RG Disney Re / B10 / 03.08."> |
| Rechnungsadresse, Letzte Zeile muss ein Länderkürzel enthalten.<br/> DE = Deutschland, GB = Großbrittannien, AT = Österreich etc. <br/> (-> [Details](#rechnungsadresse))| <textarea rows=5 placeholder="DE"></textarea> |

{{< /noteform >}}

</div>

# Arbeitsschritte

#### <input type="checkbox"/> Neue Organisation zu Pipedrive hinzufügen
#### <input type="checkbox"/> Namen nach Schema vergeben (-> [Details](#namensschema)) 
#### <input type="checkbox"/> Etikett "Rechnungsgruppe" vergeben
#### <input type="checkbox"/> Zahl der abzurechnenden Abos eintragen (-> [Details](#paketpreise))
#### <input type="checkbox"/> Zahl der unbesetzten Abos eintragen, wenn Abos unbesetzt sind
#### <input type="checkbox"/> nächsten automatischen Abrechnungstermin eintragen (-> [Details](#abrechnungstermin))
#### <input type="checkbox"/> Rechnungsadresse eintragen (-> [Details](#rechnungsadresse))


# Screencast: Rechnungsgruppe anlegen

{{< video src="rechnungsgruppe_anlegen" width="60%" muted="true" >}}

{{< include "video-explainer.md" >}}

# Details

## Namensschema

{{< include "namensschema-rechnungsgruppe.md" >}}

## Paketpreise

{{< include "paketpreise.md" >}}

## Rechnungsadresse

{{< include "rechnungsadresse.md" >}}

## Abrechnungstermin

Normalerweise erstellt ihr nach dem Anlegen einer Rechnungsgruppe sofort eine Rechnung. Wenn ihr der Automatik mitteilen wollt, dass sie deswegen für den aktuellen Abrechnungszyklus nicht aktiv zu werden braucht, könnt ihr das Datum der nächsten Abo-Abrechnung ein Jahr in die Zukunft setzen.

Beispiel: Neue Rechnungsgruppe angelegt, Start 1.1. 2024, die Rechnung wird sofort händisch geschrieben. Bei der Rechnungsgruppe steht dann aber immer noch "nächste Abo-Abrechnung: 1.1.2024", obwohl die ja schon erledigt wurde. Die Automatik wird hier eine Rechnung erstellen. Wenn ihr das Datum auf "1.1.2025" einstellt, wird die Automatik erst im kommenden Jahr aktiv.


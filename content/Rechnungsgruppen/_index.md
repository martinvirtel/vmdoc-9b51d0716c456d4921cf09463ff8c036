+++
title = " Rechnungsgruppen" 
weight = 4
+++

Rechnungsgruppen sind der zentrale Baustein der Aboverwaltung. In Pipedrive sind sie als Organisationen mit dem Etikett 'Rechnungsgruppe' angelegt. (Es gibt außerdem noch das Etikett -> 'Unternehmensgruppe'. In einer Unternehmensgruppe sind Rechnungsgruppen zusammengefasst, die zu einem Konzern gehören.). 

Mit jeder Rechnungsgruppe wird die Zahl der abzurechnenden Abos, die Zahl der unbesetzten Abos, der Abrechnungstermin, eine Rechnungsadresse und Angaben zur Rechnungsstellung (Kontaktperson, Kostenstelle etc.) gespeichert.


## Rechnungsgruppen mit Abrechnung = "Deaktiviert"

Pipedrive ist auch ein Archiv der Abo-Verwaltung. Deswegen werden alte Rechnungsgruppen, die mindestens einmal zur Abrechnung verwendet wurden, nicht gelöscht. Stattdessen wird "Abrechnug" auf "Deaktiviert" gesetzt.

Mitglieder von deaktivierten Rechnungsgruppen dürfen weiter lesen, bis sie deaktivert werden. Sie müssen ein "Deaktiviert nach"-Datum erhalten, um ihnen die Leseerlaubnis zu entziehen. Siehe  [Leseberechtigung](../Abos/#anleitung-leseberechtigung).

Kündigungen müssen außerdem mit einem Deal in der Pipeline "Kündigungen" festgehalten werden. Mehr dazu unter [Kündigung](../abos/#kündigung)




## Rechnungsgruppen mit Abrechnung = "Nicht abrechnen"

Einige Leser müssen ihre Abos nicht bezahlen. Obwohl diese Menschen keine Rechnungen erhalten, gibt es  für jede dieser Ausnahmen eigene Rechnungsgruppen, um die Leseberechtigung nachvollziehbar zu  machen.

Ausnahmen sind zum Beispiel:

  - Kolumnistinnen und Kolumnisten
  - Kolleginnen und Kollegen im Versicherungsmonitor, bei Watch Medier usw.
  - Austauschabos


### Namensschema

{{< include "namensschema-rechnungsgruppe.md" >}}


### Zahl der abzurechnenden Abos

Zahl der Abos, die bei der nächsten Verlängerung auf der Rechnung für diese Rechnungsgruppe stehen soll. Bei Kündigungen wird sie verringert, wenn neue Leser zu einer Rechnungsgruppe hinzukommen, wird sie erhöht.

{{< include "paketpreise.md" >}}

### Zahl der unbesetzten Abos

Zahl der Abos, die nicht von Mitgliedern der Rechnungsgruppe benutzt werden. Das kann passieren, wenn noch keine Ersatzleser für Kolumnisten genannt wurde, oder wenn ein Abonnent deaktivert wurde, weil er das Unternehmen verlassen hat, das Unternehmen aber noch keinen Nachrücker gemeldet hat.


### Rechnungsadresse

{{< include "rechnungsadresse.md" >}}




### Neue Rechnungsgruppe oder nicht?

Je weniger Rechnungsgruppen, desto besser. Eine neue Rechnungsgruppe ist aber nötig, wenn ein neuer Leser nicht auf einer Rechnung mit anderen Lesern abgerechnet werden kann - z.B., weil eine andere Rechnungsadresse oder Kostenstelle auf der Rechnung stehen soll. 


Wenn ein neuer Leser neu hinzukommt, muss eine Entscheidung getroffen werden: Soll er oder sie eine eigene Rechnungsgruppe erhalten? Dafür gibt es eine Entscheidungshilfe, siehe "Neuen Leser anlegen".


# Unternehmensgruppen

Rechnungsgruppen werden in einigen Fällen zu Unternehmensgruppen zusammengefasst. Das ist zum Beispiel nötig, wenn in einem Konzern zwei Tochterunternehmen jeweils ein eigenes Abo-Paket erwerben. 
